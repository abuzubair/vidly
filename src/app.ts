import express from "express";
import mongoose from "mongoose";

// Controllers (route handlers)
import * as genreController from "./controllers/genre";
import * as customerController from "./controllers/customer";
import * as bookController from "./controllers/book";

// Create Express server
const app = express();

// Express configuration
app.use(express.json());

// Connect to MongoDB
mongoose.connect('mongodb://localhost:27017/playground', { useNewUrlParser: true })
  .then(() => console.log('Connected to MongoDB...'))
  .catch((err) => console.error(err));

/**
 * Genre routes
 */
app.get("/api/genres", genreController.index);
app.post("/api/genres", genreController.create);
app.get("/api/genres/:id", genreController.findById);
app.put("/api/genres/:id", genreController.update);
app.delete("/api/genres/:id", genreController.remove);

/**
 * Genre routes
 */
app.get("/api/customers", customerController.index);
app.post("/api/customers", customerController.create);
app.get("/api/customers/:id", customerController.findById);
app.put("/api/customers/:id", customerController.update);
app.delete("/api/customers/:id", customerController.remove);

/**
 * Movie routes
 */
app.get("/api/books", bookController.index);
app.post("/api/books", bookController.create);
// app.get("/api/books/:id", bookController.findById);
// app.put("/api/books/:id", bookController.update);
// app.delete("/api/books/:id", bookController.remove);

export default app;