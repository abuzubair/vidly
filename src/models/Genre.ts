import mongoose from "mongoose";

// Genre type
export type TGenre = mongoose.Document & {
  name: string
}

// Genre schema
export const genreSchema = new mongoose.Schema({
  name: { type: String, required: true}
});

// Export Genre model
export default mongoose.model<TGenre>('Genre', genreSchema);
