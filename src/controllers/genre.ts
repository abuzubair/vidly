import validations from "../validations/genre";
import { Response, Request } from "express";
import Genre from "../models/Genre";

// Index genres
export const index = (req: Request, res: Response) => {
  const getGenres = async () => {
    try {
      const genres = await Genre.find({});
      return res.send(genres);
    } catch (err) {
      return res.status(400).send(err.message);
    }
  }

  getGenres();
};

// Create genres
export const create = (req: Request, res: Response) => {
  // Input validation
  const validationError = validations.create.validate(req.body);
  if (validationError) {
    return res.status(422).send(validationError);
  }

  // Instantiate new genre model
  const genre = new Genre({
    name: req.body.name
  });

  const createGenre = async () => {
    try {
      // check if existing genre exist
      const existingGenre = await Genre.findOne({ name: req.body.name });
      if (existingGenre) {
        return res.status(422).send(('Genre with the same name already exist.'));
      }

      // save and response new genre
      const newGenre = await genre.save()
      return res.send(newGenre);

    } catch (err) {
      return res.status(400).send(err.message);
    }
  }

  createGenre();
};

// Get genre by ID
export const findById = (req: Request, res: Response) => {
  const getGenre = async () => {
    try {
      const genre = await Genre.findById(req.params.id);
      if (!genre) {
        return res.status(404).send('Genre not found');
      }

      return res.send(genre);
    } catch (err) {
      return res.status(400).send(err.message);
    }
  }

  getGenre();
}

// Update genre
export const update = (req: Request, res: Response) => {
  // Input validation
  const validationError = validations.update.validate(req.body);
  if (validationError) {
    return res.status(422).send(validationError);
  }

  const updateGenre = async () => {
    try {
      const genre = await Genre.findByIdAndUpdate(req.params.id, {name: req.body.name}, {new: true})
      if (!genre) {
        return res.status(404).send('Genre not found');
      }

      return res.send(genre);
    } catch (err) {
      return res.status(400).send(err.message);
    }
  }

  updateGenre();
};

// Delete genre
export const remove = (req: Request, res: Response) => {
  const removeGenre = async () => {
    try {
      const genre = await Genre.findByIdAndRemove(req.params.id);
      if (!genre) {
        return res.status(404).send('Genre not found');
      }

      return res.send(genre);
    } catch (err) {
      return res.status(400).send(err.message);
    }
  }

  removeGenre();
};